//Setup the primary variables
var nconf = require( 'nconf' ),
	fork = require( 'child_process' ).fork,
	fs = require( 'fs' ),
	pkg = require( './package.json' ),
	pidFile = __dirname + '/pid',
	configFile = __dirname + '/config.json';


//Load the configuration
nconf.argv( ).file(
{

	file: configFile

} );

/**
 * 
 * Construct a new loader
 * 
 */
function Loader( )
{

	this.workers = [ ];
	this.initialize( );
	this.start( );
	
}

/**
 * 
 * Initialize the loader
 * 
 */
Loader.prototype.initialize = function( )
{
	
	//Setup a restart listener
	process.on( 'SIGHUP', this.restart );
	
	//Setup a reload listener
	process.on( 'SIGUSR2', this.reload );
	
	//Setup a stop listener
	process.on( 'SIGTERM', this.stop );
	
};

/**
 * 
 * Restart the application
 * 
 */
Loader.prototype.restart = function( )
{

	//Notify that we're restarting
	console.log( '' );
	console.log( 'Restarting!' );
	console.log( '' );
	
	//Murder the workers
	
	ApplicationLoader.killWorkers( );
	
	//Start it all back up after 10 seconds
	setTimeout( function( )
	{

		ApplicationLoader.start( );
		
	}, 10000 );
	
};

/**
 * 
 * Reload the application
 * 
 */
Loader.prototype.reload = function( )
{
	
};

/**
 * 
 * Shutdown the application
 * 
 */
Loader.prototype.stop = function( )
{

	//Notify that we're shutting down
	console.log( '' );
	console.log( 'Stopping!' );
	console.log( '' );
	
	//Murder the workers
	ApplicationLoader.killWorkers( );
	
	//Remove the pid file
	fs.unlinkSync( pidFile );
	
	//Exit
	process.exit( 0 );
	
};

/**
 * 
 * Start the application
 * 
 */
Loader.prototype.start = function( )
{
	
	//Output some messages
	console.log( '' );
	console.log( 'Andrea v%s Copyright (C) 2014-2015 ROE Host, LLC', pkg.version );
	console.log( 'This piece of software comes with ABSOLUTELY NO WARRANTY.' );
	console.log( 'This piece of software is restricted and may only be run on ROE Host infrastructure' );
	console.log( 'OR devices accepted by ROE Host, LLC' );
	console.log( '' );
	
	//Check if our pid file exists
	if( fs.existsSync( pidFile ) )
	{
		
		try
		{
		
			//Get the pid from the file
			var	pid = fs.readFileSync( pidFile, { encoding: 'utf-8' } );
			pid = pid + '';
			
			//Check that we're not just restarting
			if( pid !== ('' + process.pid ) )
			{
				
				//kill the process
				process.kill( pid, 0 );
				
				//Exit so we're not starting up in the middle of the other system shutting down
				this.stop( );
				
				return;
				
			}
		
		}
		catch (e)
		{
		
			//An error occoured. Remove the pid file
			fs.unlinkSync( pidFile );
		
		}
	
	}

	//Write the pid to file
	fs.writeFile( pidFile, process.pid );
	
	console.log(
			
			'[ApplicationLoader] Starting ' +
			this.getPorts( ).length +
			' worker' +
			( this.getPorts( ).length > 1 ? 's' : '' )
			
	);
	
	//Loop through all the ports
	for( var i = 0; i < this.getPorts( ).length; i++ )
	{
		
		//Fork a worker for each port
		this.forkWorker( i, i === 0 );
		
	}

};

/**
 * 
 * This function is used to fork a worker
 * 
 * @param index the port index to use for the worker
 * @param master wether or not this is the master process
 */
Loader.prototype.forkWorker = function( index, master )
{
	
	//Check if there's a port available
	if( !this.getPorts( )[ index ] )
	{
		
		console.log( '[ApplicationLoader] Not enough ports to fork another worker!' );
		
		return;
		
	}
	
	//Setup the variables
	process.env.master = master;
	process.env.index = index;
	process.env.port = this.getPorts( )[ index ];
	
	//Fork the worker
	var worker = fork( 'application.js', [ ],
	{
		
		silent: false,
		env: process.env
		
	} );
	
	//Setup some more variables
	worker.master = master;
	worker.index = index;
	
	//Setup the listeners for the worker
	this.setupListeners( worker );
	
	//Add the workers to the array
	this.setWorker( worker );
	
};

/**
 * 
 * This function is used to setup the listeners for a worker
 * 
 * @param worker the worker to setup the listeners for
 * 
 */
Loader.prototype.setupListeners = function( worker )
{
	
	//Store the loader instance to a variable
	var self = this;
	
	//Setup a listener for the message channel
	worker.on( 'message', function( message )
	{
		
		//Check if the message is restart
		if( message === 'restart' )
		{
			
			//Restar the application
			self.restart( );
			
		}
		
		//Check if the message is reload
		else if( message === 'reload' )
		{
			
			//Reload the application
			self.reload( );
			
		}
		
		//Check if the message is stop
		else if( message === 'stop' )
		{
			
			//Stop the application
			self.stop( );
			
		}
		
	} );
	
	//Setup a listener for the exit channel
	worker.on( 'exit', function( code, signal )
	{
		
		console.log(
				
				'[ApplicationLoader] Worker ' +
				worker.index +
				'/' +
				worker.pid +
				' has exited with code ' +
				code +
				' and signal ' +
				signal
				
		);
		
		//Check if the worker committed suicide
		if( !(worker.suicide) )
		{
			
			//Fork a new worker
			console.log( '[ApplicationLoader] Starting new worker! ' );
			self.forkWorker( worker.index, worker.master );
			
		}
		
	} );
	
};

/**
 * 
 * Kill all the workers
 * 
 */
Loader.prototype.killWorkers = function( )
{
	
	//Loop through all the workers
	this.getWorkers( ).forEach( function( worker )
	{
		
		//Tell the loader that we're murdering the worker
		worker.suicide = true;
		
		//Murder that thing. Brutally
		worker.kill( );
		
	} );
	
};

/*
 * 
 * Getters & Setters
 * 
 */

/**
 * 
 * This function is used to get the workers available
 * 
 */
Loader.prototype.getWorkers = function( )
{
	
	return this.workers;

};

/**
 * 
 * This function is used to add a worker to the available workers pool
 * 
 * @param worker the worker to add
 * @param index where to add it
 */
Loader.prototype.setWorker = function( worker, index )
{
	
	this.getWorkers( )[ index || worker.index ] = worker;

};

/**
 * 
 * This function is used to get the ports available
 * 
 * @returns an array of available ports
 */
Loader.prototype.getPorts = function( )
{
	
	var port = nconf.get( 'port' ) || [ 1362, 1363, 1364, 1365 ];
	
	if( !Array.isArray( port ) )
	{
	
		port = [ port ];
	
	}
	
	return port;
	
};

//Export the loader
module.exports = Loader;

//Setup a new loader
global.ApplicationLoader = new Loader( );
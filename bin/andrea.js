#!/usr/bin/env node

//Load the commander module
var program = require( 'commander' );

function parse( options )
{

	return options.noparse === undefined;

}

function minimal( options )
{

	return options.minimal !== undefined;

}

function favicon( options )
{
	
	return options.includefavicon !== undefined;

}

function writeColor( string )
{
	
	if( !( string.indexOf( '§' ) > -1 ) )
	{
		
		console.log( string );
		
		return;
		
	}
	
	var ansi = require( 'ansi' ),
		cursor = ansi( process.stdout );
	string = string.split( '§' );
	
	for( var i = 0; i < string.length; i++ )
	{
		
		var s = string[ i ];
		
		if( s === '' )
		{
			
			continue;
			
		}
		
		var code = s.charAt( 0 );
		s = s.slice( 1, s.length );
		
		switch( code )
		{
		
			case '0':
		
				cursor.fg.hex( '#000000' );
				
				break;
				
			case '1':
		
				cursor.fg.hex( '#0000AA' );
				
				break;
				
			case '2':
		
				cursor.fg.hex( '#00AA00' );
				
				break;
				
			case '3':
		
				cursor.fg.hex( '#00AAAA' );
				
				break;
				
			case '4':
		
				cursor.fg.hex( '#AA0000' );
				
				break;
				
			case '5':
		
				cursor.fg.hex( '#AA00AA' );
				
				break;
				
			case '6':
		
				cursor.fg.hex( '#FFAA00' );
				
				break;
				
			case '7':
		
				cursor.fg.hex( '#AAAAAA' );
				
				break;
				
			case '8':
		
				cursor.fg.hex( '#555555' );
				
				break;
				
			case '9':
		
				cursor.fg.hex( '#5555FF' );
				
				break;
				
			case 'a':
		
				cursor.fg.hex( '#55FF55' );
				
				break;
				
			case 'b':
		
				cursor.fg.hex( '#55FFFF' );
				
				break;
				
			case 'c':
		
				cursor.fg.hex( '#FF5555' );
				
				break;
				
			case 'd':
		
				cursor.fg.hex( '#FF55FF' );
				
				break;
				
			case 'e':
		
				cursor.fg.hex( '#FFFF55' );
				
				break;
				
			case 'f':
		
				cursor.fg.hex( '#FFFFFF' );
				
				break;
				
			case 'l':
				
				cursor.bold( );
				
				break;
				
			case 'n':
				
				cursor.underline( );
				
				break;
				
			case 'r':
				
				cursor.reset( );
				
				break;
				
			default:
				
				break;
		
		}
		
		cursor.write( s );
		
	}
	
	cursor.reset( );
	cursor.write( '\n' );

}

//Setup the version
program.version( require( '../package.json' ).version );

program
	.command( 'status <address>' )
	.alias( 'stat ' )
	.description( 'get the status of a server from it\'s address' )
	.option( '-p, --noparse', 'don\'t parse the status' )
	.option( '-m, --minimal', 'minimal server status reply' )
	.option( '-f, --includefavicon', 'include the favicon' )
	.action( function( address, options )
	{
		
		//Check wether or not we should parse the output
		if( parse( options ) )
		{
			
			//We only want to display this message if the output should be parsed so we're not confusing protential API's
			console.log( 'Querying %s', address );
			
		}
		
		//Set the default port
		var port = 25565;
		
		//Check if the adddress includes a port
		if( address.indexOf( ':' ) > -1 )
		{
			
			//Split the address
			address = address.split( ':' );
			
			//Set the port
			port = address[ 1 ];
			
			//Set the address
			address = address[ 0 ];
			
		}
		
		//Load the minecraft protocol
		var minecraftprotocol = require( 'minecraft-protocol' );
		
		//Setup the timeout
		var timeout = setTimeout(function()
		{
			
			//Check wether or not we should parse the output
			if( !parse( options ) )
			{
				
				//Output a nice json formattet error
				console.log(
				
						{
							
							error: true,
							code: 'TIMEOUT'
							
						}
				
				);
				
				//Return
				return;
				
			}
			
			//Tell the user the server appears to be offline
			console.log( 'The server appears to be offline!' );
			
		}, 30000);
		
		//Ping the server
		minecraftprotocol.ping(
		{
			
			host: address,
			port: port
			
		},
		function( error, response )
		{
			
			//Clear the timeout so it doesn't hang the script and outputs a silly error message that didn't happen
			clearTimeout( timeout );
			
			//Check if an error occoured
			if( error )
			{

				//Check wether or not we should parse the output
				if( !parse( options ) )
				{
					
					//Output the error in a nice JSON formatted message
					console.log(
							
							{
								
								error: true,
								result: error.code
								
							}
						
					);
					
					return;
					
				}
				
				//Try to figure out what the error is and give a nice message about it
				switch( error.code )
				{
				
					case 'ECONNREFUSED':
						
						console.log( 'Unable to connect to the server!' );
						
						break;
						
					default:
					
						console.log( 'Unkown error ' + error.code );
						
						break;
				
				}
				
				return;
				
			}
			
			//Check if we should remove the favicon or not
			if( !favicon( options ) )
			{

				//Delete the favicon as we don't need it
				delete response.favicon;
				
			}
			
			//Check wether not not we should parse the options
			if( !parse( options ) )
			{
				
				//Check wether or not we should give a minimal output
				if( minimal( options ) )
				{

					//Delete the shit we don't need
					delete response.description;
					delete response.version.protocol;
					delete response.players.sample;
					
				}
				
				//Give the response as we recieved it
				console.log( JSON.stringify( response ) );
				
				//Return so there's no further execution
				return;
				
			}
			
			//Check wether or not we should provide minimal output
			if( !minimal( options ) )
			{
				
				//Check if the MOTD contains multiple lines
				if( response.description.indexOf( '\n' ) > -1 )
				{
					
					//Split the MOTD into an array
					response.description = response.description.split( '\n' );
					
					//Yes I know there should only be 2 lines. But this is just future proofing
					for( var i = 0; i < response.description.length; i++ )
					{
						
						//Put out the MOTD line we're at
						writeColor( response.description[ i ] );
						
					}
					
				}
				else
				{
					
					//The MOTD only contains 1 line. Great. Let's output it
					writeColor( response.description );
					
				}
				
			}
			
			//Give the response
			console.log(
					
					'The server responded in ' +
					response.latency +
					'ms'
					
			);
			console.log(
			
					'The server is running ' +
					response.version.name +
					' with ' +
					response.players.online +
					' out of ' +
					response.players.max +
					' players online'
			
			);
			
			//Check wether or not we should give minimal output and that we got a sample from the server
			if( ( !minimal( options ) ) && response.players.sample !== undefined )
			{
				
				//Give a sample of the online players
				console.log( 'Here\'s a sample of who\'s online ' );
				
				//Loop through the sample array
				for( var i = 0; i < response.players.sample.length; i++ )
				{
				
					//Get the player from the array
					var player = response.players.sample[ i ];
					
					//Spit it out
					console.log(
							
							player.name +
							' with the UUID ' +
							player.id
							
					);
					
				}

			}
			
		} );
		
	} );

//Parse dem args
program.parse( process.argv );
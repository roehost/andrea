require( 'dot' ).process(
{
	
	global: '_page.render',
	destination: __dirname + '/../render/',
	path: ( __dirname + '/../templates' )

} );

var express = require( 'express' ),
	app = express( ),
	rendermanager = require( './rendermanager.js' ),
	exec = require( 'child_process' ).exec;

app.get( '/', function( req, res )
{

	res.send(rendermanager.dashboard({text:"Good morning!"}));

} );

app.get( '/query/:address/:minimal?', function( req, res )
{

	console.log( req.params );
	var command = 'andrea status ' +
		req.params.address +
		' -p' +
		( req.params.minimal !== undefined ? 'm' : '' ) +
		( req.params.minimal !== undefined ? ( req.params.minimal === 'favicon' ? 'f' : '') : '' );
	
	console.log( command );
	exec( command, function ( error, stdout, stderr )
	{
		
		console.log( typeof stdout );
		res.send( stdout );
		
	} );
	
} );

var server = app.listen( process.env.port, function ( )
{

  var host = server.address( ).address;
  var port = server.address( ).port;
  console.log(

		'[' +
		get( ) +
		'] listening at http://%s:%s', host, port );

} );
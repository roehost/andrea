Andrea
=

Andrea is a [Minecraft Server](https://minecraft.net/) monitor created by ROE Host, LLC to replace [GameCPX](http://gamecp.com)'s inbuilt server monitor.
Andrea is proudly powered by [Node.js](http://nodejs.org)

> **NOTE:**
> This software is under **HEAVY** development! So the structure of the initial release may not be the same as the current one!

----------
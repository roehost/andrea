function get( )
{
	
	return process.env.master === 'true' ? 'Master' : 'Worker #' + process.env.index;

}

function shutdown( code )
{
	
	console.log(
			
	'[' +
	get( ) +
	'] Stopping!'
	
	);
	
	process.exit( code || 0);
}

process.on( 'SIGTERM', shutdown );
process.on( 'SIGINT', shutdown );
global.get = get;

require( './lib/webserver.js' );